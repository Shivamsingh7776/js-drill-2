const data = require('./flatten');

const nestedArray = [1, [2], [[3]], [[[4]]]];

const expect_answer = [1, 2, 3, 4];

test("flatten testing", () => {
    expect(data(nestedArray)).toStrictEqual(expect_answer);
});