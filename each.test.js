const data = require('./each');

test("Testing forEach:", () => {
    items = [1,2,3,4];
    let count = 0;
    data(items, (item) =>{
        ++count;
    });
    expect(count).toStrictEqual(4);
})