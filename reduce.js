const items = [1, 2, 3, 4, 5, 5];

function reduce(elements, cb, startingValue) {
    let ans = 0;
    for (let index = 0; index < elements.length; index++) {
        ans = cb(startingValue,elements[index]);
    }
    return ans;
    
}

let answer = reduce(items, (max, element) => {
    if(max < element){
        max = element;
    }
    return max;
    // max = max + element;
    // return max;
}, 0);

console.log(answer);
module.exports = reduce;