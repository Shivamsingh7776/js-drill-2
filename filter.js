const items = [1, 2, 3, 4, 5, 5];
function filter(elements, cb) {
    let ans = [];
    
    for(let index = 0; index < elements.length; index++){
       let ans1 = (cb(elements[index]));
       if(ans1){
          ans.push(cb(elements[index])); //Key Point
       }  
    }
    return ans;
}

let answer = filter(items, (item) => {
    if(item < 4){
        return item;
    }
});

console.log(answer);

module.exports = filter;