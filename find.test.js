const items = [1, 2, 3, 4, 5, 5];
const data = require('./find');
const expect_answer = 5;

test("find method", () =>{
    expect(data(items, (item) => item > 4)).toStrictEqual(expect_answer)
});