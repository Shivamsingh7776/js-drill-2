const items = [1, 2, 3, 4, 5, 5];

const { exportAllDeclaration } = require('@babel/types');
const data = require('./filter');
const expect_answer = [1, 2, 3];

test("filter",() => {
    expect(data(items, (item) => {
        if(item < 4){
            return item;
        }
    })).toStrictEqual(expect_answer);
})