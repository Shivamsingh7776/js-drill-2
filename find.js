const items = [1, 2, 3, 4, 5, 5];

function find(elements, cb) {
    let ans;
    for(let index = 0; index < elements.length; index++){
        if(cb(elements[index]))
        {
            ans = elements[index];
            return ans;
        }

    }        
   
    return undefined;

}

let answer = find(items, (item) => item > 4);
console.log(answer);
module.exports = find;