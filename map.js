const items = [1, 2, 3, 4, 5, 5];


function map(elements, cb) {
    let ans = [];
    for(let index = 0; index < elements.length; index++){
        ans.push(cb(elements[index]));
    }
    return ans;
}

let answer = map(items, (item) =>{
    return 2 * item;
});

console.log(answer);

module.exports = map;