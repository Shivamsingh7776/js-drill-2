const data = require('./map');
const expectedAnswer = [ 2, 4, 6, 8, 10, 10 ];
const items = [1, 2, 3, 4, 5, 5];
test("map", () => {
    expect(data(items, (item) =>{
        return 2 * item;
    })).toStrictEqual(expectedAnswer);
});