// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
// function flatten(elements) {
//     let ans = [];
    
//     for(let index = 0; index < elements.length; index++){
//         if(Array.isArray(elements[index])){
//             ans = ans.concat(flatten(elements[index]));

//         }else{
//             ans.push(elements[index]);
//         }
//     }
//     return ans;
// }

// console.log(flatten(nestedArray));

const nestedArray = [1, [2], [[3]], [[[4]]]];

function flatten(elements){
    let ans = [];
    for(let index = 0; index <elements.length; index++){
        if(Array.isArray(elements[index])){
            ans = ans.concat(flatten(elements[index]));
        }
        else{
            ans.push(elements[index]);
        }
    }
    return ans;
}

console.log(flatten(nestedArray));
module.exports = flatten;