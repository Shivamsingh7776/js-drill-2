const items = [1, 2, 3, 4, 5, 5];
const data = require('./reduce');
const expect_answer = 5;
test("reduce testing", () =>{
    expect(data(items, (max, element) => {
        if(max < element){
            max = element;
        }
        return max;
        // max = max + element;
        // return max;
    }, 0)).toStrictEqual(expect_answer);
});