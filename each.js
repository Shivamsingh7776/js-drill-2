const items = [1, 2, 3, 4, 5, 5];


function each(elements, cb) {
    let ans = [];
    for(let index = 0;index < elements.length; index++){
        ans = cb(elements[index]);
    }
    return ans;
}

let answer = each(items, (item) =>{
    return item;
});
console.log(answer);
module.exports = each;